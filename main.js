// 1. forEach створює callback функцію для кожного елемента масива.
// 2. Найпростішим методом для очистки масива є arr.splice(0).
// 3. Це робиться за допомогою команди Array.isArray.

const filterBy = (arr, dataType) => {return arr.filter((element) => {return typeof element !== dataType})}

console.log(filterBy([23, "23", null, 'hello', 'World'], "number"))

